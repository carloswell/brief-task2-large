import React from 'react'
import Footer from '../components/footer'
import Icon from '../components/icons'

/*..footer container file with the footer layout ready to be export to contactContent.js page.. */

export function FooterContainer(){
    return(
        <Footer>
            <Footer.Wrapper>
                <Footer.Row>
                <Footer.Column>
                    <Footer.Title>Contact Us for Free Quote</Footer.Title>
                    <Footer.Text>Email:</Footer.Text>
                    <Footer.Link href="#">jack.lumber@edgetreesurgery.biz</Footer.Link>
                </Footer.Column>

                <Footer.Column>
                    <Footer.Title>Call Us on:</Footer.Title>
                    <Footer.Link href="#">01234-567-8900</Footer.Link>
                    <Footer.Link href="#">9876-543-210</Footer.Link>
                </Footer.Column>

                <Footer.Column>
                    <Footer.Title>Address:</Footer.Title>
                    <Footer.Text>1 Edge Hill,</Footer.Text>
                    <Footer.Text>St Helens Road</Footer.Text>
                    <Footer.Text>Ormiskirk L39 4QP</Footer.Text>   
                </Footer.Column>

                <Footer.Column>
                    <Footer.Title>Find Us:</Footer.Title>
                    <Footer.Link href="#"><Icon className="fab fa-facebook-square"/></Footer.Link>  <Footer.Link href="#"><Icon className="fab fa-instagram-square"/></Footer.Link>  <Footer.Link href="#"><Icon className="fab fa-twitter"/></Footer.Link>
                </Footer.Column>

                <Footer.Column>
                    <Footer.Text>&copy;Copyright/2022 Edge Tree Surgery</Footer.Text>
                </Footer.Column>
                </Footer.Row>
            </Footer.Wrapper>
        </Footer>
    )
}