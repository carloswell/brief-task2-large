
/*..app.js file to connect the components to the main id root on index.html.. */

import React from 'react';
import Navbar from './components/Navbar/Navbar';
import './App.css';
import Home from './pages/Home'
import About from './pages/About'
import Contact from './pages/Contact';
import Services from './pages/Services';
import { BrowserRouter  as Router, Route, Switch } from 'react-router-dom';   /* ..import router and switch from react router dom dependencies..*/
import {FooterContainer} from "./containers/footer"

 /* ..Main app component function to deal with the pages templates and route links using switch component ..*/
function App() {
  return (
    <div className="App">

      <Router>   {/* ..Main router wrapp all components and organize they linked between each other ..*/}

          <Navbar/>
        <Switch>   {/* ..switch component wrap all the other page component which need to be linked ..*/}

          <Route exact path="/">  {/* ..route for each page component with thier respective links path..*/}
            <Home/>
          </Route>
          <Route  path="/about">
            <About/>
          </Route>
          <Route  path="/contact">
            <Contact/>
          </Route>
          <Route  path="/services">
            <Services/>
          </Route>
          
        </Switch> 
          <FooterContainer/>
      </Router>    
    </div>
  );
}

export default App;    /* ..export app component to the index.js with root id..*/
