import React from "react";
import "./Banner.css";
import quote from '../img/quotation.svg'
import quot from '../img/quotationv.svg'
import { Link } from 'react-router-dom';

/*..Main hero banner component from the home page..*/

function Banner() {

    function changeBackground(e){
        e.currentTarget.src = quot;  /*..function change background to change the images in the USPs sign in the hero banner..*/

    }

    function out(e) {
        e.currentTarget.src = quote; /*..function to return first image after user mouse out of the USPs sign..*/

 
    }
    return(
        <section className="slider">
                <h2>Storm Damage:</h2>
                <p>24 Hours Emergency</p>
                <p>Call. 01234-567-800</p>
            <p className="slog">Quick Services, no job too small or too large.</p>
            
           <Link className="lin" to="#"><img src={quote}  onMouseOver={changeBackground} onMouseOut={out} alt="quotation bar slogan" /></Link> 
        </section>
        
    )

};

export default Banner;  /*..Export the heor banner component to the home.js file..*/
