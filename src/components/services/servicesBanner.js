import React from "react";
import { Link } from 'react-router-dom';
import { Button } from '../Buttons'
import './servicesBanner.css'

/*..Hero banner development for the services pages.. */

function ServicesBanner() {

    return(
        <section className="services-s">
            <section className="emerg">
                <h2>Storm Damage:</h2>
                <p>24 Hours Emergency</p>
                <p>Call. 01234-567-800</p>
            </section>        

            <section className="service-slog">
                <p >We offer a Free no obligation quote, which also 
                    comes with complimentary advisory service,
                    where we will recommend any works, we deem necessary.
                </p>
            </section>

            <section className="service-lin">
                    <Link  to="#"> <Button  onClick={()=> {console.log("You clicked on me!")}}
                    type="button" buttonStyle="btn--primary--solid"
                    buttonSize ="btn--large"
                    >Free Quotation Now!</Button></Link> 
            </section>

        </section>
        
    )

};

export default ServicesBanner;  /*..export the ServicesBanner component to the services content.. */