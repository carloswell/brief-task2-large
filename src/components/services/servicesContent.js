import React from "react";
import './servicesContent.css';
import Simg1 from '../../img/s-img1.png'
import Simg2 from '../../img/s-img2.png'
import Simg3 from '../../img/s-img3.png'
import Simg4 from '../../img/s-img4.png'
import Simg5 from '../../img/s-img5.png'
import Simg6 from '../../img/s-img6.png'
import Simg7 from '../../img/s-img7.png'
import Simg8 from '../../img/s-img8.png'

/*..ServicesContent component for the services page..  */

function ServicesContent() {
    return(

        <article className='services-container' > { /*..main article hold all parts of the component..  */}

            <h1 className="s-title">Our Services</h1>

            <section className='service-top'> { /*..first section of the services content ..  */}

              <figure className="sh simg1">  
                <img src={Simg1} alt="tree pruning image" />
                <h2>Tree Pruning</h2>
                <p>Ensure optimum</p>
                <p>tree health.</p>
              </figure>    

              <figure className="sh simg2">  
                <img src={Simg2} alt="tree felling image" />
                <h2>Tree Felling</h2>
                <p>Remove the whole</p>
                <p>tree to ground zero.</p>
              </figure>
              
              <figure className="sh simg3">  
                <img src={Simg3} alt="crown thinning image" />
                <h2>Crown Thinning</h2>
                <p>Selective removal of complete.</p>
                <p>branches back to the main stem.</p>
                <p>to reduce density of foliage.</p>
              </figure>

              <figure className="sh simg4">  
                <img src={Simg4} alt="stump grinding image" />
                <h2>Stump Grinding</h2>
                <p>Remove the tree stump</p>
                <p>below ground zero.</p>
              </figure>

            </section>

            <hr></hr>    { /*..hr tag hold the line to separety the sections..  */}

            <section className='service-bottom'> { /*..second section of the serevices content..  */}
                <figure className="sh simg5">  
                    <img src={Simg5} alt="tree pruning image" />
                    <h2>Pollarding</h2>
                    <p>The removal of 100% of</p>
                    <p>the crowns foliage back to</p>
                    <p>new or old pollard points</p>
                </figure>    

                <figure className="sh simg6">  
                    <img src={Simg6} alt="tree felling image" />
                    <h2>Tree Reduction</h2>
                    <p>Overall crown reduction</p>
                    <p>shaping, makes the overall</p>
                    <p>size, height and spread small</p>
                    <p>while giving the tree a </p>
                    <p>pleasant shape.</p>
                </figure>
                
                <figure className="sh simg7">  
                    <img src={Simg7} alt="crown thinning image" />
                    <h2>Deadwooding</h2>
                    <p>Remove of dead wood</p>
                    <p>and crown cleaning.</p>
                </figure>

                <figure className="sh simg8">  
                    <img src={Simg8} alt="stump grinding image" />
                    <h2>Chemical Treatment</h2>
                    <p>Application of chemicals for</p>
                    <p>pest and disease control for</p>
                    <p>trees and poisoning</p>
                    <p>unwanted tree stumps.</p>
                </figure>

            </section>
           
        </article>
    );
}

export default ServicesContent;  { /*..export the servicesContent to the services.js component..  */}

