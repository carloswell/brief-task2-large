import React from "react";
import {Button} from './Buttons'
import { useState } from "react";
import '../components/contactContent.css';
import Map from '../img/west-map.jpg'

     /* Main contact content component ..*/

function ContactContent() {

    const [name, setName, email, setEmail] = useState(" ");
    return(

        <article className='contact-container' >      {/* main article hold the content parts..*/}
            
            <h1 className="title">Contact Us</h1>

            <section className='contact-left'>    {/* left side of the cotact content..*/}
                    <h2>Get in Touch For a Free Quotation:</h2>
                    <p>Drop us a message below with your enquire.</p>

                    <form>     {/* form tag to hold the input parts of the contact content..*/}
                        <label >
                            <span className="required">Name:</span>
                            <input type="text" name={name} onChange={(e) =>
                                 setName(e.target.value)}/>
                        </label>

                        <label>
                        <span className="required">Email:</span>
                            <input type="Email" name={email} onChange={(e) =>
                                 setEmail(e.target.value)}/>
                        </label>

                        <label>
                        <span className="required">Enquire:</span>
                            <select >
                            <option value="select"></option>
                            <option value="tree pruning">Tree Pruning</option>
                            <option value="tree felling">Tree Felling</option>
                            <option value="crown thinning">Crown Thinning</option>
                            <option value="stump grinding">Stump Grinding</option>
                            <option value="pollarding">Pollarding</option>
                            <option value="tree reduction">Tree Reduction</option>
                            <option value="deadwooding">Deadwooding</option>
                            <option value="chemical treatment">Chemical Treatment</option>
                            </select>
                        </label>

                        <label>
                        <span className="required">Phone:</span>
                            <input type="number" name="phone"/>
                        </label>

                        <label>
                        <span className="required">Message:</span>
                            <textarea/>

                        </label>


                    </form>
                    <section className="btContact">        {/* Contact content button using the component button created..*/}     
                        <Button  onClick={()=> {console.log("You clicked on me!")}}
                        type="button" buttonStyle="btn--secondary--outline"
                        buttonSize ="btn--large"
                        >SUBMIT</Button>          
                    </section>

           
                    <section className='contact-right'>
                        <h3>Alternatively you can contact us by:</h3>
                        <strong>Email:</strong>
                        <p>jack.lumber@edgetreesurgery.biz</p>

                        <strong>Call Us on:</strong>
                        <p>01234 567 890</p>
                        <p>09876 543 210</p>

                        <strong>Address:</strong>
                        <p>1 Edge Hill</p>
                        <p>St Helens Road</p>
                        <p>Ormiskirk L39 4QP</p>

                        <strong>Area We Cover:</strong>
                         <img src={ Map} alt="west lancachire map"/>    

                         <strong>Serving West Lancashire areas</strong>
                        <p>Ormskirk, Burscough, Rufford,</p>
                        <p>Southport, Aughton, Skelmersdale</p>
   
                    </section>

            </section>

        </article>
    );
}

export default ContactContent;    {/* export the contact content  component to the contact js component..*/}