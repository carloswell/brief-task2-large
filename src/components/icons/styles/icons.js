import styled from 'styled-components'

/*..used the styled components dependencies to create a js styling for icons use i tag */

export const Icon = styled.i`
    font-size: 18px;
    margin-right: 16px;
`