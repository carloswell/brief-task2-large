import React from "react";
import { Icon } from'./styles/icons' /*..import icon styling from folder style file icons.. */

/*..Socila media icons return the class name to be choose with the Icon tag.. */

export default function Icons({className}){
    return<Icon className={className} />
}