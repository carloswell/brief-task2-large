/*..customize components for the link navigation  as a array of  keys and values of the navigation bar component items... */

export const MenuItems = [
    {
        title: 'Home',
        url: '/',
        cName: 'nav-links'
    },
    {
        title: 'About Us',
        url: '/about',
        cName: 'nav-links'
    },
    {
        title: 'Contact Us',
        url: '/contact',
        cName: 'nav-links'
    },
    {
        title: 'Services',
        url: '/services',
        cName: 'nav-links'
    }

  

 
 
]