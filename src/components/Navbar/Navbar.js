import React, { Component } from "react";
import { MenuItems } from "./MenuItems"
import './Navbar.css'
import logo from '../../img/logo.svg'
import { Link } from 'react-router-dom'; /*..Link using react router dependencies for a fast response in the browser*/

/*..Navigation bar components main content with upper line, logo and the links to be export to the main app.js file*/
class Navbar extends Component {
    state = { clicked: false } /*..settle the menu link to false by default till the user clicks in the bars icon */

    handleClick = () => {
        this.setState({clicked: !this.state.clicked })  /*..function handleClick to handle the open and close of menu links when the screen is under 960px */
    }
    render() {
        return(
            <nav className="NavbarItems">
                <section className="up-line">
                    Serving West Lancashire areas: Ormiskirk, Burscough, Rufford, Southport,
                    Aughton, Skelmersdale. <span><a href="#">CALL(+44)01234-567-890</a></span>

                </section>

                <Link to="/" className="link"> <img src={logo} alt="logo tree"/></Link>
                <Link to="/" className="link">  <h1 className="navbar-logo"> Edge Tree Surgery</h1></Link>   
                    
                <section className="menu-icon" onClick={this.handleClick}>   {/*..using the handleClick function to open and close menu links with menu bars when is under 960px */}
                    <i className={this.state.clicked ? 'fas fa-times' : 'fas fa-bars'}></i>
                </section>
                <ul className={this.state.clicked ? 'nav-menu active' : 'nav-menu'}>  {/*..handle the menu open and close using css styling with class nav-menu and nav-menu active..*/}
                    {MenuItems.map((item, index) => {
                         return (
                            <li key={index}>
                                <Link className={item.cName} to={item.url}>  {/*..return  pair of keys and values items from components menuItems.. */}
                                 {item.title}   
                                </Link>
                            </li>

                         )   
                    })}

                </ul>

            </nav>
        )
    }
}

export default Navbar  /*..export the navigation bar component.. */