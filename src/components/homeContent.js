import React from 'react'
import './homeContent.css'
import { Link } from 'react-router-dom';
import { Button } from './Buttons'
import Img1 from '../img/himg1.jpg'
import Img2 from '../img/himg2.jpg'
import Img3 from '../img/himg3.jpg'

 /* ..main home content component function..*/

function HomeContent() {
    return(

        <article className='container' >  { /* ..main article hold all the component parts..*/}

            <section className='left'>   { /* ..section hold the left side part of the page component..*/}
                <h3>Welcome to Edge Tree Surgery</h3>
                <p>We are some of the most highly skilled and reliable, tree surgeons around wesy Lancashire area.
                    With over 25 years' experience in tree services, each one of our clients knows there are in safe 
                    hands with our trusted company.
                </p>
                
                    <h3>Tree Surgeons</h3>
                    <p>We offer a free no obligation quote, which also comes with complimentary advisory service. 
                        we will recommend any works we deem necessary. We can organise planing applications for 
                        any tree protected by tree preservation orders.
                    </p>
                    <Button className="btOne" onClick={()=> {console.log("You clicked on me!")}}     
                    type="button" buttonStyle="btn--primary--solid"
                    buttonSize ="btn--large"
                    >Free Quotation Now!</Button>          { /* ..button component again used as production..*/} 
            </section>

            <section className='right'>
                <h4>We Perform:</h4>
                
                <figure className='img1'>
                    <img src={Img1} alt="tree removal"/>
                    <p>Tree Removal</p>
                </figure>
                <figure className='img2'>
                    <img src={Img2} alt="tree removal"/>
                    <p>Tree Pruning</p>

                </figure>
                <figure className='img3'>
                    <img src={Img3} alt="tree removal"/>
                    <p>Tree Felling</p>

                </figure>

                <h4 className='sec'>And Many More.</h4>

            </section>
            <section>
                <Link to="/services"><Button onClick={()=> {console.log("You clicked on me!")}}
                    type="button" buttonStyle="btn--secondary--outline"
                    buttonSize ="btn--large"
                    >Check Our Services
                </Button></Link>            { /* ..button component used as production secondary button..*/}
            </section>
        </article>
    );
}

export default HomeContent;   { /* ..export defautl home content to the home.js component..*/}