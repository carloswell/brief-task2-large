import React from "react";
import './Buttons.css';

 /* button component parts for the website pages..*/

const STYLES =[
    "btn--primary--solid",
    "btn--secondary--outline",
];

const SIZES = ["btn--large", "btn--small"];

export const  Button = ({   /* export button component to the website pages as Button, below the function parameters to be used in production..*/

    children, type, onClick,
    buttonStyle, 
    buttonSize
}) => {

    const checkButtonStyle = STYLES.includes(buttonStyle)
     ? buttonStyle : STYLES[0];

     const checkButtonSize = SIZES.includes(buttonSize) 
     ? buttonSize : SIZES[0];

    return(
       <button className={`btn ${checkButtonStyle} ${checkButtonSize}`}
             onClick={onClick} type={type}>
                {children}
        </button>
    )
};

