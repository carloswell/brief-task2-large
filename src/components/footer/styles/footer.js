import styled from 'styled-components'

/*..footer component with style file using react style components dependenses..*/ 

export const Container = styled.div`
    padding:10px;
    background-color:#104A2F;
    margin-left:0px;

`

export const Wrapper = styled.div`
display: flex;
flex-direction: column;
justify-content: start;
max-width: 1000px;
margin: 0 auto;    
`

export const Column = styled.div`
display: flex;
flex-direction: column;
text-align:left;
margin-left:0;
margin-top: 20px;

`


export const Row = styled.div`
    display:grid;
    grid-template-columns: repeat(auto-fill, minmax(230px, 1fr));
    grid-gap: 10px;
   
   
`

export const Link = styled.a`
    display:inline-table;
    color: black;
    margin-bottom:10px;
    font-size: 15px;
    text-decoration: none;

    &:hover{
        color: #67A925;
        transition: 200ms ease-in;
    }
`

export const Title = styled.div`
   font-size: 16px;
   color: black;
   margin-bottom: 10px;
   font-weight: bold;
`
export const Text = styled.div`
    font-size: 14px;

`