import React from "react";
import './aboutContent.css';
import {Button} from './Buttons'    /*..import button component from buttons.js compoent..*/

 /*..component parts of about mian content ..*/

function AboutContent() {
    return(

        <article className='about-container' >  { /*..Main article hold all the component parts..*/}
            <h1 className="title">About Us</h1>

            <section className='top1'>
                <h3>Our Approach</h3>
                <p>With over 25 years' experience in arboriculture. we applying professional 
                    approach to all your Tree Surgery needs to rejuvenate your outdoor spaces,
                    with that improving your living space.
                </p>
                </section>

                <section className='top2'>
                    <h3>MORE ABOUT EDGE TREE SURGEONS:</h3>
                    <p>Edge Tree surgeons primary aim, according first with public safety is to 
                        maintain tree health. Managing and sustaining the future of the trees in 
                        the north west of england. The managing Director Jack Lumber, with 24 years
                        working experience in arbiculture services, has a wide set of skills and experience in the field.
                    </p>
                </section>

            <section className="botom1">
                    <h3>Our Services</h3>
                    <p>We offer a free no obligation quote. Which also comes with complimentary
                        advisory service. We will recommend any works we deem necessary. We can 
                        organise planning applications for any trees protected by tree preservation
                        orders.
                    </p>
            </section>

            <section className="botom2">

                    <h3>Arboriculture training Completed by Edge Tree Surgeons:</h3>
                    <ul>
                    <li><b>CS30</b> Chainsaw Maintenance and Cross Cutting.</li>
                    <li><b>CS31</b> Chainsaw-felling of small Trees.</li>
                    <li><b>CS38</b> Climb Trees and Perform Ariel Rescue.</li>
                    <li><b>CS39</b> Operate a chain saw from a rope and harness .</li>
                    <li><b>CS41</b> Undertake Sectional Felling.</li>
                    <li><b>NPTC</b> level 2 certificate in manually fed wood chipper operations.</li>
                    <li><b>City & Guilds</b> phase 2 arboriculture.</li>
                    <li><b>First Aid Training.</b></li>
                    </ul>
            </section>    

            <section className="btAbout">   { /*..button component imported from buttons.js component..*/}
                    <Button  onClick={()=> {console.log("You clicked on me!")}}
                    type="button" buttonStyle="btn--primary--solid"
                    buttonSize ="btn--large"
                    >Free Quotation Now!</Button>          
            </section>
           
        </article>
    );
}

export default AboutContent;  { /*..export about content component to the about.js page ..*/}