import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

http://localhost:3000/  /* ..using locahost port 3000 ..*/

ReactDOM.render(
        <App />
    , document.getElementById('root')    /* ..pass to index.html by the id root ..*/
);
