import React from "react";
import AboutContent from "../components/aboutContent";   /* ..import about content component..*/

 /* ..about page main component fucntion..*/

function About() {
    return(
        <section>
            <AboutContent/>     {/* ..using about content component inside..*/}
        </section>
    )
}

export default About;   /* ..export about component to the app.js component..*/
