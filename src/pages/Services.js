import React from "react";
import ServicesBanner from '../components/services/servicesBanner';   /* ..import banner hero component..*/
import ServicesContent from '../components/services/servicesContent';    /* ..import services content component..*/

function Services() {
    return(
        <section>
            <ServicesBanner/>        {/* ..using services banner component inside..*/}
                <ServicesContent/>    {/* ..using services content component inside..*/}
        </section>
    )
}

export default Services;      /* ..export services component to the app.js component..*/