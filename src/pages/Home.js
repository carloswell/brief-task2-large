import React from "react";
import Banner from "../components/Banner";     /* ..import banner hero component..*/
import HomeContent from "../components/homeContent";    /* ..import home content component..*/

 /* ..home page main component function..*/

function Home() {
    return(

        <section className="content">
            <Banner/>          {/* ..using banner component inside..*/}
            <HomeContent/>   {/* ..using home content component inside..*/}
        </section>
    );
}

export default Home;   /* ..export home component to the app.js component..*/