import React from "react";
import ContactContent from '../components/contactContent'  /* ..import contact content component..*/

 /* ..contact page main component function..*/

function Contact() {
    return(
        <section>
            <ContactContent/>     {/* ..using contact content component inside..*/}
        </section>
    )
}

export default Contact;  /* ..export contact component to the app.js component..*/